package com.guillaumevdn.gcore;

import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import javax.annotation.Nullable;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import com.guillaumevdn.gcore.lib.chat.JsonMessage;
import com.guillaumevdn.gcore.lib.collection.CollectionUtils;
import com.guillaumevdn.gcore.lib.collection.LowerCaseHashMap;
import com.guillaumevdn.gcore.lib.compatibility.Version;
import com.guillaumevdn.gcore.lib.compatibility.material.CommonMats;
import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.concurrency.RWHashMap;
import com.guillaumevdn.gcore.lib.concurrency.RWWeakHashMap;
import com.guillaumevdn.gcore.lib.element.struct.container.typable.TypableElementType;
import com.guillaumevdn.gcore.lib.element.struct.container.typable.TypableElementTypes;
import com.guillaumevdn.gcore.lib.legacy_npc.NPCManager;
import com.guillaumevdn.gcore.lib.legacy_npc.NpcProtocols;
import com.guillaumevdn.gcore.lib.number.NumberUtils;
import com.guillaumevdn.gcore.lib.object.ObjectUtils;
import com.guillaumevdn.gcore.lib.player.MineToolsUtils;
import com.guillaumevdn.gcore.lib.player.PlayerUtils;
import com.guillaumevdn.gcore.lib.plugin.PluginUtils;
import com.guillaumevdn.gcore.lib.reflection.ReflectionObject;
import com.guillaumevdn.gcore.lib.string.StringUtils;
import com.guillaumevdn.gcore.lib.string.Text;
import com.guillaumevdn.gcore.lib.string.placeholder.Replacer;
import com.guillaumevdn.gcore.lib.tuple.Pair;
import com.guillaumevdn.gcore.lib.tuple.Triple;
import com.guillaumevdn.gcore.lib.wrapper.Wrapper;
import com.guillaumevdn.gcore.lib.wrapper.WrapperString;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

/**
 * @author GuillaumeVDN
 */
public class WorkerGCore {

    // ----- npc manager
    private NPCManager npcManager = null;

    public WorkerGCore() {
        try {
            if (Version.ATLEAST_1_9 && PluginUtils.isPluginEnabled("ProtocolLib")) {
                new NpcProtocols(); // init npc protocols ; this sets the instance field if success
                (npcManager = new NPCManager()).enable();
                GCore.inst().getMainLogger().info("Enabled NPC manager with ProtocolLib");
            }
        } catch (Throwable exception) {
            GCore.inst().getMainLogger().error("Couldn't enable NPC manager with ProtocolLib", exception);
            npcManager = null;
        }
    }

    public NPCManager getNpcManager() {
        return npcManager;
    }

    // ----- offline players
    private final LowerCaseHashMap<Pair<UUID, String>> offlinePlayersUUIDs = new LowerCaseHashMap<>(10, 0.75f);
    private final RWHashMap<UUID, String> offlinePlayersNames = new RWHashMap<>(10, 0.75f);

    public void registerOfflinePlayer(String name, UUID uuid) {
        if (name != null) {
            offlinePlayersUUIDs.put(name, Pair.of(uuid, name));
        }
    }

    public Pair<UUID, String> getOfflinePlayer(String name) {
        return offlinePlayersUUIDs.get(name);
    }

    public Stream<String> getOfflinePlayersNamesLowercase() {
        return offlinePlayersUUIDs.keySet().stream();
    }

    public Stream<String> getOfflinePlayersNames() {
        return offlinePlayersUUIDs.values().stream().map(Pair::getB);
    }

    public String getOrFetchOfflinePlayerNameAsync(OfflinePlayer player) {
        final WrapperString wrapper = WrapperString.of("<?>"); // ewww
        getOrFetchOfflinePlayerName(player, name -> {
            wrapper.set(name);
        });
        return wrapper.get();
    }

    public void getOrFetchOfflinePlayerName(OfflinePlayer player, Consumer<String> callback) {
        String name = player.getName();
        if (name == null) {
            name = offlinePlayersNames.get(player.getUniqueId());
        }
        if (name != null) {
            callback.accept(name);
        } else {
            fetchProfile(player.getUniqueId(), null, null, null, profile -> {
                final String n = profile != null && profile.getName() != null ? profile.getName() : "<name?>";
                offlinePlayersNames.put(player.getUniqueId(), n);
                callback.accept(n);
            });
        }
    }

    @Nullable
    public UUID getOrFetchOfflinePlayerUUIDAsync(String name) {
        final Wrapper<UUID> wrapper = Wrapper.of(null); // ewww
        getOrFetchOfflinePlayerUUID(name, uuid -> {
            wrapper.set(uuid);
        });
        return wrapper.get();
    }

    public void getOrFetchOfflinePlayerUUID(String name, Consumer<UUID> callback) {
        fetchProfile(null, name, null, null, profile -> {
            final UUID uuid = profile != null ? profile.getId() : null;
            final String n = profile != null && profile.getName() != null ? profile.getName() : "<name?>";
            offlinePlayersNames.put(uuid, n);
            callback.accept(uuid);
        });
    }

    // ----- await inputs
    private final RWWeakHashMap<Player, Pair<Consumer<String>, Runnable>> awaitingChats = new RWWeakHashMap<>(1, 1f);
    private final RWWeakHashMap<Player, Object> awaitingLocationsCancelChat = new RWWeakHashMap<>(1, 1f);
    private final RWWeakHashMap<Player, Triple<Consumer<Location>, Runnable, Long>> awaitingLocations = new RWWeakHashMap<>(1, 1f); // third is the timestamp
                                                                                                                                    // when it was asked ;
                                                                                                                                    // because the editor GUI
                                                                                                                                    // button to trigger this is
                                                                                                                                    // often shift + click, the
                                                                                                                                    // GUI is closed and so the
                                                                                                                                    // player shifts
                                                                                                                                    // automatically quickly ;
                                                                                                                                    // set a 1s delay
    private final RWWeakHashMap<Player, Object> awaitingItemsCancelChat = new RWWeakHashMap<>(1, 1f);
    private final RWWeakHashMap<Player, Pair<Consumer<ItemStack>, Runnable>> awaitingItems = new RWWeakHashMap<>(1, 1f);

    public boolean hasAwaitingChat(Player player) {
        return player != null && awaitingChats.containsKey(player);
    }

    public Pair<Consumer<String>, Runnable> consumeAwaitingChat(Player player) {
        return awaitingChats.remove(player);
    }

    public boolean hasAwaitingLocationCancelChat(Player player) {
        return player != null && awaitingLocationsCancelChat.containsKey(player);
    }

    public boolean consumeAwaitingLocationCancelChat(Player player) {
        return awaitingLocationsCancelChat.remove(player) != null;
    }

    public Triple<Consumer<Location>, Runnable, Long> consumeAwaitingLocations(Player player) {
        Triple<Consumer<Location>, Runnable, Long> awaiting = awaitingLocations.get(player);
        if (awaiting != null && System.currentTimeMillis() - awaiting.getC() > 1000L) {
            awaitingLocations.remove(player);
            return awaiting;
        }
        return null;
    }

    public boolean hasAwaitingItemCancelChat(Player player) {
        return player != null && awaitingItemsCancelChat.containsKey(player);
    }

    public boolean consumeAwaitingItemCancelChat(Player player) {
        return awaitingItemsCancelChat.remove(player) != null;
    }

    public Pair<Consumer<ItemStack>, Runnable> consumeAwaitingItems(Player player) {
        return awaitingItems.remove(player);
    }

    public void awaitChat(Player player, Text message, Consumer<String> onChat, Runnable onCancel) {
        // cancel current
        Pair<Consumer<String>, Runnable> currentChat = awaitingChats.remove(player);
        if (currentChat != null && currentChat.getB() != null)
            currentChat.getB().run();

        // ask
        GCore.inst().operateSync(() -> {
            player.closeInventory();
        });
        if (message != null) {
            message.replace("{cancel}", () -> TextGeneric.textCancel.parseLine()).send(player);
        }
        awaitingChats.put(player, Pair.of(onChat, onCancel));
    }

    public void awaitChatWithSuggestedValue(Player player, Text message, String suggestValue, Consumer<String> onChat, Runnable onCancel) {
        // cancel current
        Pair<Consumer<String>, Runnable> currentChat = awaitingChats.remove(player);
        if (currentChat != null && currentChat.getB() != null)
            currentChat.getB().run();

        // ask
        GCore.inst().operateSync(() -> {
            player.closeInventory();
        });
        if (suggestValue != null && !suggestValue.isEmpty()) {
            JsonMessage json = new JsonMessage();
            json.append(message.replace("{cancel}", () -> TextGeneric.textCancel.parseLine()).parseLine()).setSuggest(suggestValue).build();
            json.send(player);
        } else {
            message.replace("{cancel}", () -> TextGeneric.textCancel.parseLine()).send(player);
        }
        awaitingChats.put(player, Pair.of(onChat, onCancel));
    }

    public void awaitChatOrNone(Player player, Text message, String noneText, Consumer<String> onChat, Runnable onCancel) {
        awaitChat(player, message, raw -> {
            onChat.accept(raw.equalsIgnoreCase(noneText) ? null : raw);
        }, onCancel);
    }

    public void awaitChatInteger(Player player, Text message, Consumer<Integer> onChat, Runnable onCancel) {
        awaitChatInteger(player, message, Integer.MIN_VALUE, Integer.MAX_VALUE, onChat, onCancel);
    }

    public void awaitChatInteger(Player player, Text message, int min, int max, Consumer<Integer> onChat, Runnable onCancel) {
        awaitChat(player, message, raw -> {
            final Integer integer = NumberUtils.integerOrNull(raw);
            if (integer == null) {
                TextGeneric.messageInvalidNumber.replace("{value}", () -> raw).send(player);
                awaitChatInteger(player, message, min, max, onChat, onCancel);
                return;
            }
            if (integer < min) {
                TextGeneric.messageInvalidNumberRangeMin.replace("{value}", () -> raw).replace("{min}", () -> min).send(player);
                awaitChatInteger(player, message, min, max, onChat, onCancel);
                return;
            }
            if (integer > max) {
                TextGeneric.messageInvalidNumberRangeMax.replace("{value}", () -> raw).replace("{max}", () -> max).send(player);
                awaitChatInteger(player, message, min, max, onChat, onCancel);
                return;
            }
            onChat.accept(integer);
        }, onCancel);
    }

    public <E extends Enum<E>> void awaitChatEnum(Player player, Text message, Class<E> enumClass, Consumer<E> onChat, Runnable onCancel) {
        awaitChat(player, message, raw -> {
            final E value = ObjectUtils.safeValueOf(raw, enumClass);
            if (value == null) {
                TextGeneric.messageInvalidEnum.replace("{value}", () -> raw)
                        .replace("{values}", () -> StringUtils.toTextString(", ", CollectionUtils.asList(enumClass.getEnumConstants()))).send(player);
                awaitChatEnum(player, message, enumClass, onChat, onCancel);
                return;
            }
            onChat.accept(value);
        }, onCancel);
    }

    public <T extends TypableElementType> void awaitChatType(Player player, Text message, TypableElementTypes<T> types, Consumer<T> onChat, Runnable onCancel) {
        awaitChat(player, message, raw -> {
            final T value = types.safeValueOf(raw);
            if (value == null) {
                TextGeneric.messageInvalidEnum.replace("{value}", () -> raw).replace("{values}", () -> StringUtils.toTextString(", ", types.values()))
                        .send(player);
                awaitChatType(player, message, types, onChat, onCancel);
                return;
            }
            onChat.accept(value);
        }, onCancel);
    }

    public void awaitChatMat(Player player, Text message, Consumer<Mat> onChat, Runnable onCancel) {
        awaitChat(player, message, raw -> {
            final Mat value = Mat.firstFromIdOrDataName(raw).orElse(null);
            if (value == null) {
                TextGeneric.messageInvalidEnumNoList.replace("{value}", () -> raw).send(player);
                awaitChatMat(player, message, onChat, onCancel);
                return;
            }
            onChat.accept(value);
        }, onCancel);
    }

    public void awaitLocation(Player player, Text message, Consumer<Location> onSelect, Runnable onCancel) {
        awaitLocation(player, message, Replacer.GENERIC, onSelect, onCancel);
    }

    public void awaitLocation(Player player, Text message, Replacer messageReplacer, Consumer<Location> onSelect, Runnable onCancel) {
        // cancel current
        Pair<Consumer<String>, Runnable> currentChat = awaitingChats.remove(player);
        Triple<Consumer<Location>, Runnable, Long> currentLocation = awaitingLocations.remove(player);
        if (currentChat != null && currentChat.getB() != null)
            currentChat.getB().run();
        if (currentLocation != null && currentLocation.getB() != null)
            currentLocation.getB().run();
        // ask
        if (message != null) {
            message.replace(messageReplacer).replace("{cancel}", () -> TextGeneric.textCancel.parseLine()).send(player);
        }
        awaitingLocations.put(player, Triple.of(onSelect, onCancel, System.currentTimeMillis()));
        awaitingLocationsCancelChat.put(player, "");
    }

    public void awaitItem(Player player, Text message, Consumer<ItemStack> onSelect, Runnable onCancel) {
        // cancel current
        Pair<Consumer<String>, Runnable> currentChat = awaitingChats.remove(player);
        Pair<Consumer<ItemStack>, Runnable> currentLocation = awaitingItems.remove(player);
        if (currentChat != null && currentChat.getB() != null)
            currentChat.getB().run();
        if (currentLocation != null && currentLocation.getB() != null)
            currentLocation.getB().run();
        // ask
        if (message != null) {
            message.replace("{cancel}", () -> TextGeneric.textCancel.parseLine()).send(player);
        }
        awaitingItems.put(player, Pair.of(onSelect, onCancel));
        awaitingItemsCancelChat.put(player, "");
    }

    // ----- game profile / skull items
    private static final Pattern USERNAME_PATTERN = Pattern.compile("\\w{3,16}");
    private static final Predicate<String> USERNAME_MATCHER = string -> USERNAME_PATTERN.matcher(string).matches();
    private RWHashMap<UUID, GameProfile> profileCache = new RWHashMap<>(10, 1f);

    private static GameProfile defaultProfile(UUID ownerUUID, String ownerName) {
        return gameProfile(ownerUUID != null ? ownerUUID : UUID.randomUUID(), ownerName != null ? ownerName : "Unknown",
                "ewogICJ0aW1lc3RhbXAiIDogMTYwODAzMTQ1MTk2MSwKICAicHJvZmlsZUlkIiA6ICJlYzU2MTUzOGYzZmQ0NjFkYWZmNTA4NmIyMjE1NGJjZSIsCiAgInByb2ZpbGVOYW1lIiA6ICJBbGV4IiwKICAic2lnbmF0dXJlUmVxdWlyZWQiIDogdHJ1ZSwKICAidGV4dHVyZXMiIDogewogICAgIlNLSU4iIDogewogICAgICAidXJsIiA6ICJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzFhNGFmNzE4NDU1ZDRhYWI1MjhlN2E2MWY4NmZhMjVlNmEzNjlkMTc2OGRjYjEzZjdkZjMxOWE3MTNlYjgxMGIiCiAgICB9CiAgfQp9",
                "J4QNnTc0p9NbhK5zkD5pd+N2lKtH/0y884MOFQyxVGcUYDuSaa5XkkoLBTe/iaOICjarfwd1gLgNNg8XqAW3imb7bsOlN1D+3A3POkjlrdTKgLqFU9ouGwhdhh6rbMa6Sz6Ir6b8bgbeniEKYQxzOjyLbZwaDfJgXycPuQ7dnXiycVrgMYAcSHv3FH/K2Fm4RfjeIWJctWWsgpZdxmX9E0o83LEKlqEH6bT1aMTVnWJDRcak9A/OR6iSwz6ABrsWzARtlwi10mVwZUEQovByOo+UHxGfQErWm6kXbn7U/faDI3Gfq3ovvP/KyhGjB64gYQN0OWFt99N8FM+jWnPuRxVZlH0jx0Sxe2PGPvNy/lwD4gDbJfKScMSsapYZqbTenZ4QakqPVfGYI23JdQMC3IcTjuz4hHlKNjF+AgGZEqz/gDyKUT+95eOJH+8Kr0+KCzmKaL2zKY1/or7zcCsaeAyY/M+trfr6nARfFVBInHVYLHkOPkRSj3xvjNKW1sP4szJvxhQ/V968ipydRTlnQ67H8J8Laz5TDxxB2uQlRkGi6bvk1T7LSNNY/GSTovJVatR9adxTjbndby+DmrfFb666XjZ6kJshwEsudnQs2BU/jG9zi3tvCKoma/d6LbcSr2hfSYCl+ErWCFDSuVB4zJZa5rOLGW2Ea5s1ePFeHiM=");
    }

    public void fetchProfile(@Nullable UUID ownerUUID, @Nullable String ownerName, Consumer<GameProfile> callback) {
        fetchProfile(ownerUUID, ownerName, null, null, callback);
    }

    public void fetchProfile(@Nullable UUID ownerUUID, @Nullable String ownerName, @Nullable String skinData, @Nullable String skinSignature,
            Consumer<GameProfile> callback) {
        if (ownerName != null && !USERNAME_MATCHER.test(ownerName)) {
            ownerName = null;
        }

        // has data already
        if (skinData != null) {
            callback.accept(gameProfile(ownerUUID, ownerName, skinData, skinSignature));
        }
        // no data, fetch it
        else if (ownerUUID != null || ownerName != null) {
            // disabled in config, don't fetch at all
            if (ConfigGCore.dontFetchPlayerProfiles) {
                callback.accept(defaultProfile(ownerUUID, ownerName));
                return;
            }

            // determine UUID to use for fetching
            final UUID correctUUID;
            if (Bukkit.getOnlineMode()) {
                // ... online mode : either use said UUID or attempt to check server name cache
                if (ownerUUID != null) {
                    correctUUID = ownerUUID;
                } else {
                    final OfflinePlayer pl = PlayerUtils.getOfflineWhoPlayedBefore(ownerName);
                    correctUUID = pl != null ? pl.getUniqueId() : null;
                }
            } else {
                // ... offline mode : force fetching of UUID if has a name
                correctUUID = ownerName != null ? null : ownerUUID;
            }

            // no correct UUID, fetch it from name
            if (correctUUID == null) {
                if (ownerName == null) {
                    callback.accept(defaultProfile(ownerUUID, ownerName));
                } else {
                    final String ownerNameF = ownerName;
                    GCore.inst().operateAsync(() -> {
                        final UUID fetchedUUID = MineToolsUtils.fetchUUID(ownerNameF);
                        fetchProfileByUUID(fetchedUUID != null ? fetchedUUID : ownerUUID, ownerNameF, callback);
                    }, error -> {
                        error.printStackTrace();
                        callback.accept(defaultProfile(ownerUUID, ownerNameF));
                    });
                }
                return;
            }

            // has correct UUID, use it
            final GameProfile cached = profileCache.get(correctUUID);
            if (cached != null) {
                callback.accept(cached);
            } else {
                fetchProfileByUUID(correctUUID, ownerName, callback);
            }
        }
        // no data and nothing to fetch from
        else {
            callback.accept(defaultProfile(ownerUUID, ownerName));
        }
    }

    private void fetchProfileByUUID(UUID correctUUID, @Nullable String originalName, Consumer<GameProfile> callback) {
        GCore.inst().operateAsync(() -> {
            GameProfile profile = correctUUID == null ? null : MineToolsUtils.fetchProfile(correctUUID); // nullable
            if (profile == null) {
                profile = defaultProfile(correctUUID, originalName);
            }
            profileCache.put(correctUUID, profile);
            callback.accept(profile);
        }, error -> {
            error.printStackTrace();
            callback.accept(null);
        });
    }

    private static GameProfile gameProfile(UUID ownerUUID, String ownerName, String skinData, String skinSignature) {
        final GameProfile profile = new GameProfile(ownerUUID != null ? ownerUUID : UUID.randomUUID(), ownerName != null ? ownerName : "SomeGuy");
        profile.getProperties().put("textures", skinSignature != null ? new Property("textures", skinData, skinSignature) : new Property("textures", skinData));
        return profile;
    }

    public void buildPlayerHead(OfflinePlayer owner, String name, List<String> lore, Consumer<ItemStack> done) {
        Consumer<UUID> run = uuid -> {
            fetchProfile(uuid, owner.getName(), null, null, profile -> {
                try {
                    ItemStack item = CommonMats.PLAYER_HEAD.newStack();
                    SkullMeta meta = (SkullMeta) item.getItemMeta();
                    ReflectionObject.of(meta).setField("profile", profile);
                    if (name != null)
                        meta.setDisplayName(name);
                    if (lore != null)
                        meta.setLore(lore);
                    item.setItemMeta(meta);
                    done.accept(item);
                } catch (Throwable error) {
                    GCore.inst().getMainLogger().error("An error occured when building head item for " + owner.getName(), error);
                }
            });
        };
        if (!Bukkit.getOnlineMode()) { // fetch UUID by name if it's an offline server, to get the correct skin
            GCore.inst().operateAsync(() -> {
                UUID uuid = /* MojangUtils */MineToolsUtils.fetchUUID(owner.getName());
                run.accept(uuid != null ? uuid : owner.getUniqueId());
            });
        } else {
            run.accept(owner.getUniqueId());
        }

    }

    // ----- static
    public static WorkerGCore inst() {
        return GCore.inst().getWorler();
    }

}
